<?php

$myBooleanVar = "0";

if($myBooleanVar)
{
    echo '$myBooleanVar is true<br>';
}
else
{
    echo '$myBooleanVar is false<br>';
}
//string example start

$myVar=4848;

$myStr='Hello world $myVar <br>';
echo $myStr;

$heredocVar = <<<BITM
hsakjdhjhfgdsjhfgdsjkfhgdsjkfgdsjk
dsdsfdsfdsafdsafdsafdsafds
fdsfdsafadsfsda
fdsfdsaffffffffffffdsa
ewfefe $myStr
fewfewfewfefefe
fewfefefe
efefefe
fdsfasf
BITM;

echo "$heredocVar <br>";

$myStr='Hello world $myVar <br>';


$nowdocVar = <<<'BITM'
hsakjdhjhfgdsjhfgdsjkfhgdsjkfgdsjk
dsdsfdsfdsafdsafdsafdsafds
fdsfdsafadsfsda
fdsfdsaffffffffffffdsa
ewfefe $myStr
fewfewfewfefefe
fewfefefe
efefefe
fdsfasf
BITM;

echo "$nowdocVar <br>";






//string ex end



//array ex

$myArray= array(13,true,75.25,"Hello",5);

echo "<pre>";
print_r($myArray);//see only data
var_dump($myArray);//data & info

echo "</pre>";

$personAge = array("Rahim"=>35,"kahim"=>45,"sahim"=>25,60.25);
echo $personAge['Rahim']."<br></br>";
print_r($personAge);

//arr ex end